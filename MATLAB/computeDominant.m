%% Compute the histogram of the box tracking

fprintf('\n%-25s \t %-3s \t %-10s \n','CLASS CREATION','-->','START');

ii = 1;

while hasFrame(video{1,1})
    
    frame = readFrame(video{1,1});
    deleteBackgroundExternal;
    FF = faceDetector(framePF);
    
    %loop for each face found in the frame
    for kdx = 1:size(FF,1)
        
        currDominantColor = [0, 0, 0];
        bestClass = 0;
        bestDist = Inf;
        currBox = FF(kdx,:);
        face = imcrop(frame, currBox);
        
        %create rectangle on the chest
        boxChest = zeros(1,4);
        boxChest(1) = max([0, currBox(1) - currBox(3)*(face2chest-1)/2]);
        boxChest(2) = min([pixelH, currBox(2)+currBox(4)+face2Neck*currBox(4)]);
        boxChest(3) = face2chest*currBox(3);
        
        if boxChest(1) + boxChest(3) > pixelW
            
            boxChest(3) = pixelW - boxChest(1) - 1;
            
        end
        
        boxChest(4) = face2chest*currBox(4);
        
        if boxChest(2) + boxChest(4) > pixelH
        
            boxChest(4) = pixelH - boxChest(2) - 1;
        
        end
        
        boxChest = floor(boxChest);
        chest = imcrop(frame,boxChest);
        
        % compute dominant color of the chest
        chestR = reshape(chest,size(chest,1)*size(chest,2),3);
        [clusters,centroids] = kmeans(double(chestR),3);
        dominantCluster = mode(clusters);
        currDominantColor = round(centroids(dominantCluster,:));
        
        k = 0;
        
        while k < numClass
            
            if abs(camsHistLocal(1,numClass-k).Colordominant - currDominantColor) < thDominant
                
                if active(numClass-k)  % same dominant color
                    
                    currDist = norm(double(boxChest(1:2) - camsHistLocal(1,numClass-k).PrevBox));   % current distance
                    
                    if currDist < bestDist
                        
                        bestDist = currDist;
                        bestClass = numClass - k;
                        
                    end
                    
                end
                
            end
            
            k = k + 1;
            
        end
        
        if bestClass > 0 && bestDist < spatialDist % update existing class
                        
            camsHistLocal(1,bestClass).Colordominant = ((1 - wCurrCol) * camsHistLocal(1,bestClass).Colordominant + wCurrCol * currDominantColor);
            camsHistLocal(1,bestClass).PrevBox = boxChest(1:2);
            inactiveFrames(bestClass) = 0;
            name = strcat('ID', num2str(ii), '.jpg');
            imwrite(face, strcat(repo,num2str(bestClass),'\',name));
            
        else % create new class
            
            numClass = numClass + 1;
            
            camsHistLocal(1,numClass).Colordominant = currDominantColor;
            camsHistLocal(1,numClass).PrevBox = boxChest(1:2);
            mkdir(repo,num2str(numClass));
            name = strcat('ID', num2str(ii), '.jpg');
            imwrite(face, strcat(repo,num2str(numClass),'\',name));
            
            inactiveFrames(numClass) = 0;
            active(numClass) = true;
            
        end
        
    end
    
    for nn = 1:numClass  % update classes which may be false or not active any longer
        
        if active(nn) % take into account active classes not updated for some frames
                        
            if inactiveFrames(nn) <= inactiveFramesBeforeDeactivation
                
                inactiveFrames(nn) = inactiveFrames(nn)+1;
                
            else
                
                active(nn) = ~active(nn);
                
                if length(dir(strcat(repo,num2str(nn)))) > numPhoto
                    
                    numPeople = numPeople + 1;
                    camsHist(1,numPeople).Colordominant = camsHistLocal(1,nn).Colordominant;
                    camsHist(1,numPeople).Colorparticle = randi([0 255],1,3);
                    fisherfaces;
                    
                else
                    
                    rmdir(strcat(repo,num2str(nn)),'s'); % if false class, destroy it
                    
                end
                
            end
            
        end
        
    end
    
    ii = ii+1;
    
end

fprintf('%-25s \t %-3s \t %-10s \n','CLASS CREATION','-->','FINISH');