%-------------------------------------------------------------------------%
%                 INITIALIZATION FOR CALIBRATION                          %
%-------------------------------------------------------------------------%

% Rotation Matricies and Translation Vector structure
relativePoses = struct('RotoTranslation', ...
                        struct('RotMatrix',[],...
                               'TransVector',[]));
