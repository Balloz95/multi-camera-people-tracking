%% Clear the workspace and the command window

clearvars
clc
close all

flag = false;
disp("1 --> ONE-FILTER IMPLEMENTATION")
disp("2 --> TWO-FILTER IMPLEMENTATION")

while(~flag)
    
    in = input('Choose filter implementation: ', 's');
    
    if (strcmp(in, '1') || (strcmp(in, '2')))
        
        flag = true;
        
    end
    
end

if strcmp(in, '1')
    
    %% Create directory for faces detection
    
    createRepo;
    
    %% Cameras Initialization
    
    setCameras;
    
    %% Initialize structures and matrix
    
    initializeAll;
    
    %% Communication and Vision Graphs
    
    graphsCreation;
    
    %% Compute the backgrounds
    
    backgroundSubtraction;
    
    %% Compute dominant color and centroid of each person
    
    computeDominant;
    
    %% Particle Filter
    
    % Reset the video
    for ii = 2:numCam
        
        video{1,ii}.CurrentTime = 0;
        
    end
    
    % Initialization of face and color filter
    initializePF;
    
    % Main loop
    videoPlayer.release();
    frameOld = zeros(pixelH,pixelW,3,'uint8');
    
    if strcmp(writeOtputVideo,'y')
        
        open(videoWriter);
        
    end
    
    if numPeople > 0
        
        while hasFrame(video{1,jj})
            
            frame = readFrame(video{1,jj});
            deleteBackground;
            updateParticles;
            logLikelihood;
            resampleParticles;
            createBoxPF;
            checkBoxEdgesLocation;
            lostFace;
            chooseCam;
            showParticles;
            
            if mod(jj,numCam) == 0
                
                jj = 2;
                videoPlayer([frameOld,frame]);
                
                if strcmp(writeOtputVideo,'y')
                    
                    writeVideo(videoWriter,[frameOld,frame]);
                    
                end
                
            else
                
                jj = jj+1;
                frameOld = frame;
                
            end
            
            numF = numF + 1;
            
        end
        
    end
    
    if strcmp(writeOtputVideo,'y')
        
        close(videoWriter);
        
    end
    
elseif strcmp(in, '2')
    
    %% Create directory for faces detection
    
    createRepo;
    
    %% Initialize structures and matrix
    
    initializeAll;
    
    %% Cameras Initialization
    
    setCameras;
    
    %% Communication and Vision Graphs
    
    graphsCreation;
    
    %% Compute the backgrounds
    
    backgroundSubtraction;
    
    %% Compute dominant color and centroid of each person
    
    computeDominant;
    
    %% Particle Filter
    
    % Reset the video
    for ii = 2:numCam
        
        video{1,ii}.CurrentTime = 0;
        
    end
    
    % Initialization of face and color filter
    initializePF1;
    
    % Main loop
    
    videoPlayer.release();
    frameOld = zeros(pixelH,pixelW,3,'uint8');
    
    if strcmp(writeOtputVideo,'y')
        
        open(videoWriter);
        
    end
    
    if numPeople > 0
        
        while hasFrame(video{1,jj})
            
            frame = readFrame(video{1,jj});
            deleteBackground;
            updateParticles1;
            logLikelihood1;
            resampleParticles1;
            createBoxPF1;
            checkBoxEdgesLocation;
            lostFace;
            chooseCam1;
            showParticles1;
            
            if mod(jj,numCam) == 0
                
                jj = 2;
                step(videoPlayer,[frameOld,frame]);
                
                if strcmp(writeOtputVideo,'y')
                    
                    writeVideo(videoWriter,[frameOld,frame]);
                    
                end
                
            else
                
                jj = jj+1;
                frameOld = frame;
                
            end
            
            numF = numF + 1;
            
        end
        
    end
    
    close(videoWriter);
end
