close all
% Create figure
figure1 = figure('WindowState','maximized');

% Create axes
axes1 = axes('Parent',figure1);
plot(1:length(state(2,2).enables),state(2,2).enables,'sr','MarkerSize',4,'MarkerFaceColor','r','LineWidth',1);
hold on
plot(1:length(state(3,2).enables),state(3,2).enables-.02*ones(1,length(state(3,2).enables)),'sb','MarkerSize',4,'MarkerFaceColor','b','LineWidth',1);
state(2,2).callMW(state(2,2).callMW == 0) = -10;
state(3,2).callMW(state(3,2).callMW == 0) = -10;
state(2,2).callCC(state(2,2).callCC == 0) = -10;
state(3,2).callCC(state(3,2).callCC == 0) = -10;
plot(1:length(state(2,2).enables),state(2,2).callMW*1.4,'dr','MarkerSize',10,'MarkerFaceColor','r','LineWidth',1);
plot(1:length(state(2,2).enables),state(2,2).callCC*.4,'or','MarkerSize',10,'MarkerFaceColor','r','LineWidth',1);
plot(1:length(state(3,2).enables),state(3,2).callMW*1.6,'db','MarkerSize',10,'MarkerFaceColor','b','LineWidth',1);
plot(1:length(state(3,2).enables),state(3,2).callCC*.6,'ob','MarkerSize',10,'MarkerFaceColor','b','LineWidth',1);
axis([0 length(state(2,2).enables) -.1 3])
xlabel({'Frame'},'Interpreter','latex');
box(axes1,'on');
% Set the remaining axes properties
set(axes1,'FontSize',20,'TickLabelInterpreter','latex','YTick',[0 1 2],...
    'YTickLabel',{'Disabled','Enabled (no face)','Enabled (face)'},...
    'YTickLabelRotation',60);