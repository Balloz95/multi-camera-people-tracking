fprintf('%-25s \t %-3s \t %-10s\n','GRAPHS CREATION','-->','START');

if numCam > 1
    
    insideCams = numCam-1;
    V = zeros(insideCams);
    
    for ii = 1 : insideCams
    
        for k = 1 : insideCams
        
            if abs(ii-k) == 1
            
                V(ii,k) = 1;
            
            end
            
        end
        
    end

    V(1,insideCams)=1; %the last node is always connected 
    V(insideCams,1)=1; %to the first one
    
    diagV = diag(diag(V)); %avoiding self loops
    V = V-diagV;

    names = {};
    
    for ii = 1 : insideCams
    
        names{ii} = strcat('cam',num2str(ii)); %labelling the cams
    
    end

    namesC = ['master' names]; %labelling of the external cam
    n = ones(insideCams, 1); % adding the master node connected to
    m = ones(1,numCam);      % all the others
    C = [n V];
    C = [m; C];
    diagC = diag(diag(C)); %avoiding self loops
    C = C-diagC;

    G_V = graph(V,names); %vision graph
    G_C = graph(C,namesC); %communication graph
    
else %numcam = 1
    
    G_V = graph([0],{'master'});
    G_C = G_V;
    
end


%% Plot the ommunication and the vision graph
figure()
plot(G_V)
title('Vision Graph')

fprintf('%-25s \t %-3s \t %-10s\n','GRAPHS CREATION','-->','FINISH');
