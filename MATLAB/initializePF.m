for k = 2:numCam
    
    for p = 1:numPeople
        
        state(k,p).X(1,:) = randi(pixelW, 1, N_particles);
        state(k,p).X(2,:) = randi(pixelH, 1, N_particles);
        state(k,p).X(3:4,:) = zeros(2, N_particles);
        state(k,p).preEnable = false(1,numCam);
        state(k,p).enable = true;
        state(k,p).foundFace = false;
        state(k,p).lostFace = 0;
        
    end
    
end