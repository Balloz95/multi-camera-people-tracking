%-------------------------------------------------------------------------%
%                 INITIALIZATION FOR BACKGROUND                           %
%-------------------------------------------------------------------------%

% Number of frame to compute the background
numFrameBg = 50;

% Color for the background
colorBG = [-150, -150, -150];

% Logic matrix used to trasform the background color into colorBG
diff = zeros(pixelH, pixelW,3);

% Kernel for closing the logical difference matrix
closeKernel = strel('square', 10);