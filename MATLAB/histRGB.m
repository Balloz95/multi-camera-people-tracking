function dominant = histRGB(I)
    if (size(I, 3) ~= 3)
        error('Input image must be RGB.')
    end

    nBins = 256;
    [rHist, ~] = imhist(I(:,:,1), nBins);
    [gHist, ~] = imhist(I(:,:,2), nBins);
    [bHist, ~] = imhist(I(:,:,3), nBins);
    [~, rMode] = max(rHist);
    [~, gMode] = max(gHist);
    [~, bMode] = max(bHist);

    dominant = [rMode-1 gMode-1 bMode-1];
    
end