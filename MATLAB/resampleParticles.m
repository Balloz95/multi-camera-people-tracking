L2 = zeros(numPeople,N_particles);
for p = 1:numPeople
    
    if state(jj,p).enable
        
        if length(unique(L(p,:))) == 1
            
            L(p,1) = 1e-10;
            
        end
        
        L2(p,:) = exp(L(p,:) - max(L(p,:)));
        Q = L2(p,:) / sum(L2(p,:), 2);
        R = cumsum(Q, 2);
        
        % Generating Random Numbers
        
        N = size(state(jj,p).X, 2);
        T = rand(1, N);
        
        % Resampling
        try
            [~, I] = histc(T, R);
        end
            
        state(jj,p).X = state(jj,p).X(:, I + 1);
        
    end
    
end
