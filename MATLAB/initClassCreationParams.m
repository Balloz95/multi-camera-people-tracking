%-------------------------------------------------------------------------%
%                 INITIALIZATION FOR CREATION OF THE CLASSES              %
%-------------------------------------------------------------------------%

% Size of each sample face for performing Fisherfaces
sampleSide = 30;

% Samples per class
numSamples = 10;