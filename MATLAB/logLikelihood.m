Npix_h = pixelH;
Npix_w = pixelW;

N = N_particles;

L = zeros(numPeople,N);
Y = framePF;
Y = permute(Y, [3 1 2]);

A = -log(sqrt(2 * pi) * Xstd_rgb);
B = - 0.5 / (Xstd_rgb.^2);

currFacesBoxes = faceDetector(framePF);
numCurrFaces = size(currFacesBoxes,1);
updateFilterFace = false(1,numPeople);

for face = 1:numCurrFaces
    
    currFace = imcrop(frame,currFacesBoxes(face,:));
    currFace = rgb2gray(currFace);
    currFace = imresize(currFace,[sampleSide, sampleSide]);
    currFace = reshape(currFace, sampleSide^2, 1);
    currFace = projMatrix'*double(currFace);
    
    currBestFilterMatch = 0;
    minFaceDist = faceMatchTh;
    
    if numPeople == 1
        
        currBestFilterMatch = 1;
        
    else
    
        for p = 1:numPeople % chooses the best match among all the currently detected faces
            
            if state(jj,p).enable
                
                currFaceDist = norm(currFace-camsHist(1,p).Centroid)/sqrt(norm(currFace)*norm(camsHist(1,p).Centroid));
                
                if currFaceDist < minFaceDist && currFaceDist < faceMatchTh % threshold --> possible not to have suitable matches
                    
                    minFaceDist = currFaceDist;
                    currBestFilterMatch = p;
                    
                end
                
            end
            
        end

    end
    
    if logical(currBestFilterMatch)
        
        updateFilterFace(1,currBestFilterMatch) = true;
        state(jj,currBestFilterMatch).foundFace = true;
        
        for k = 1:N
            
            m = state(jj,currBestFilterMatch).X(1,k); % x cohordinate
            n = state(jj,currBestFilterMatch).X(2,k); % y cohordinate
            
            I = (m >= 1 & m <= Npix_h);
            J = (n >= 1 & n <= Npix_w);
            
            if I && J
                
                currColor = double(Y(:, m, n));
                
                currColorDiff = currColor' - camsHist(1,currBestFilterMatch).Colordominant;  % the more a particle has a different colour, the more is penalized
                
                currColorDiffNorm = currColorDiff * currColorDiff';
                
                currSpatialDiff = 0;
                
                if n > currFacesBoxes(face,1) + currFacesBoxes(face,3)*(face2chest-1)/2
                    
                    currSpatialDiff = n - (currFacesBoxes(face,1) + currFacesBoxes(face,3)*(face2chest-1)/2);
                    
                else
                    
                    if currFacesBoxes(face,1) -  currFacesBoxes(face,3)*(face2chest-1)/2 > n
                        
                        currSpatialDiff = currFacesBoxes(face,1) -  currFacesBoxes(face,3)*(face2chest-1)/2 - n;
                        
                    end
                    
                end
                
                if currFacesBoxes(face,2) + currFacesBoxes(face,4) + face2Neck*currFacesBoxes(face,4) > m  % above the neck
                    
                    currSpatialDiff = currSpatialDiff + (currFacesBoxes(face,2) + currFacesBoxes(face,4) + face2Neck*currFacesBoxes(face,4) - m);
                    
                else
                    
                    if m > currFacesBoxes(face,2) + currFacesBoxes(face,4) + face2Neck*currFacesBoxes(face,4) + face2chest*currFacesBoxes(face,4) % below the chest
                        
                        currSpatialDiff = currSpatialDiff + (m - (currFacesBoxes(face,2) + currFacesBoxes(face,4) + face2Neck*currFacesBoxes(face,4)...
                            + face2chest*currFacesBoxes(face,4)));
                        
                    end
                    
                end
                
                currTotDiff = wColour*currColorDiffNorm + (1-wColour)*currSpatialDiff^3;  % inserire anche minFaceDist?
                L(currBestFilterMatch,k) =  A + B * currTotDiff;
                
            else
                
                L(currBestFilterMatch,k) = -Inf;
                
            end
            
        end
        
    end
    
end

for p = 1:numPeople
    
    if state(jj,p).enable
        
        if ~updateFilterFace(1,p)
            
            state(jj,p).foundFace = false;
            
            for k = 1:N
                
                m = state(jj,p).X(1,k); % x cohordinate
                n = state(jj,p).X(2,k); % y cohordinate
                
                I = (m >= 1 & m <= Npix_h);
                J = (n >= 1 & n <= Npix_w);
                
                if I && J
                    
                    currColor = double(Y(:, m, n));
                    
                    currColorDiff = currColor' - camsHist(1,p).Colordominant;  % the more a particle has a different colour, the more is penalized
                    
                    currColorDiffNorm = currColorDiff * currColorDiff';
                    
                    currTotDiff = wColour*currColorDiffNorm + (1-wColour)*defaultSpatialDiff;
                    
                    L(p,k) =  A + B * currTotDiff;
                    
                else
                    
                    L(p,k) = -Inf;
                    
                end
                
            end
            
        end
        
    end
    
end

