%% Lost Face

for p = 1:numPeople
    
    if state(jj,p).enable
        
        if ~state(jj,p).foundFace
            
            state(jj,p).lostFace = state(jj,p).lostFace - 1;
            
            if state(jj,p).lostFace == 0
                
                existCandidateCamera = false;
                
                % look for any preactivated camera
                for n = 2:numCam
                    
                    if state(jj,p).preEnable(n)

                        state(n,p).enable = true;
                        state(n,p).lostFace = numFrameLostFace;
                        existCandidateCamera = ~existCandidateCamera;
                        break
                        
                    end
                    
                end
                
                if ~existCandidateCamera
                      
                    %Most Wanted
                    for kk = 2:numCam
                        
                        if logical(V(jj-1,kk-1))
                            
                            state(kk,p).enable = true;
                            state(kk,p).lostFace = numFrameLostFace;
                            state(kk,p).X(1,:) = randi(pixelW, 1, N_particles);
                            state(kk,p).X(2,:) = randi(pixelH, 1, N_particles);
                            state(kk,p).X(3:4,:) = zeros(2, N_particles);
                            
                        end
                        
                    end
                    
                end
                
            end            
            
        else
            
            state(jj,p).lostFace = numFrameLostFace;
            
        end
        
    end
    
end