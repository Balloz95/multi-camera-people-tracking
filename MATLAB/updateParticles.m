N = N_particles;

for p = 1 : numPeople
    if state(jj,p).enable
        state(jj,p).X = F_update * state(jj,p).X;
        state(jj,p).X(1:2,:) = round(state(jj,p).X(1:2,:) + Xstd_pos * randn(2, N));
        state(jj,p).X(3:4,:) = state(jj,p).X(3:4,:) + Xstd_vec * randn(2, N);
    end
end