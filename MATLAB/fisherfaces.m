%% Training set

d = dir(repo);
X = zeros(numPeople*numSamples,sampleSide^2);

for i = 1:numPeople
    
    personDir = dir(strcat(d(i+2,1).folder,'\',d(i+2,1).name));
    decimationStep = round((size(personDir,1)-2)/numSamples);
    
    for k = 1:decimationStep:size(personDir,1)-2
        
        image = imread(strcat(personDir(k+2,1).folder,'\',personDir(k+2,1).name));
        image = rgb2gray(image);
        image = imresize(image,[sampleSide, sampleSide]);
        image = reshape(image, 1, sampleSide^2);
        X((i-1)*numSamples+fix(k/decimationStep)+1,:) = image;
        
    end
    
end

%% Fisherfaces algorithm

% mean faces computation
mean_all_faces = mean(X);
class_mean_face = cell(numPeople,1);

for class = 1:numPeople
    
    class_mean_face(class) = {mean(X((class-1)*numSamples+1:class*numSamples,:))};
    
end

% between-class covariance matrix
class_mean_matrix = zeros(numPeople,size(X,2));

for class = 1:numPeople
    
    class_mean_matrix(class,:) = class_mean_face{class,1};
    
end

mean_all_matrix = ones(numPeople,1)*mean_all_faces;
Sb = (class_mean_matrix-mean_all_matrix)'*(class_mean_matrix-mean_all_matrix);

% within-class covariance matrix
Sw = zeros(size(X,2));

for class = 1:numPeople
    
    Xclass = X((class-1)*numSamples+1:class*numSamples,:)-ones(numSamples,1)*class_mean_face{class,1};
    Sw = Sw + Xclass'*Xclass;
    
end

% projection matrix
X_centered = X - ones(size(X,1),1)*mean_all_faces;
[Wpca,~] = eigs(X_centered*X_centered', numPeople*(numSamples-numPeople+1));
Wpca = X_centered'*Wpca;
[Wfld,~] = eigs(Wpca'*Sb*Wpca,Wpca'*Sw*Wpca,numPeople-1);
projMatrix = Wpca*Wfld;
projMatrix = normalize(projMatrix, 1, 'norm');

% training data projection

for class = 1:numPeople
    
    camsHist(1,class).Centroid = projMatrix'*class_mean_face{class,1}';
    
end