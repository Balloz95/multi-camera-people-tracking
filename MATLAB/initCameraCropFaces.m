%-------------------------------------------------------------------------%
%                 INITIALIZATION OF THE CROP FACES                        %
%-------------------------------------------------------------------------%

% Threshold for the X axis
thX = 80;

% Threshold for the Y axis
thY = 60;

% Multiplying coefficent for computing neck height from face size
face2Neck = .5;

% Multiplying coefficent for computing chest size from face size
face2chest = 2;

% Initialize the Face Detector
faceDetector = vision.CascadeObjectDetector;

% Set the minimum and the maximum area for the face box
faceDetector.MinSize = [20 20];
faceDetector.MaxSize = [40 40];
