particles = zeros(N_particles,numPeople);

for p = 1:numPeople
    
    if state(jj,p).enable
        
        particles(:,1:2) = [state(jj,p).X(2,:); state(jj,p).X(1,:)]';
        
        if ~state(jj,p).foundFace
            
            confidenceColor = 'y';
            
        else
            
            confidenceColor = 'g';
            
        end
        
        try
            frame = insertObjectAnnotation(frame, 'rectangle', state(jj,p).box, strcat(num2str(p), ' detected'),'Color',confidenceColor,'LineWidth',5);
        end
        
    end
    
end