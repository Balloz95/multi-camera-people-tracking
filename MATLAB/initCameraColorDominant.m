%-------------------------------------------------------------------------%
%                 INITIALIZATION FOR HISTOGRAMS                           %
%-------------------------------------------------------------------------%

% In the beginning we have only one person that comes into the room
numPeople = 0;

% Threshold for the pixels foreground
th = 50; 

% Threshold for the dominant color
thDominant = 30;

% Weight of the current colour for the class dominant colour update
wCurrCol = .3;

% Structure which contains the data to compute the dominat color of each
% person                
camsHist = struct('Colordominant', zeros(1,3),...
                  'Centroid', 0);
              
% Projection matrix to compute the compressed image
projMatrix = [];
              
% Number of class
numClass = 0;

% Spatial distance between faces box
spatialDist = 50;

% Number of elements inside each faces folder
numPhoto = 52;

% Number of frames which a class is not updated for
inactiveFrames = zeros();

% Flag indicating if a person still has to be classified
active = false();

% Maximum waited frames for delete or confirm one class
inactiveFramesBeforeDeactivation = 60;

% Structure for saving the class parameters
camsHistLocal = struct('Colordominant', [],...
                       'PrevBox',[]);