fprintf('\n%-25s \t %-3s \t %-10s\n','INITIALIZE CAMERAS','-->','START');

% Set the number of cameras
prompt = 'Number of the cameras in the room: ';
numCam = input(prompt);
prompt = 'Insert the name of the test folder: ';
testName = input(prompt,'s');
d = dir(strcat('test/',testName));

%Loop to insert the name of the videos
for ii = 1:numCam
    
    videoName = strcat('test/',testName,'/',d(ii+2).name);
    videoLocal = VideoReader(videoName);
    video{ii} = videoLocal;

end

fprintf('%-25s \t %-3s \t %-10s \n','INITIALIZE CAMERAS','-->','FINISH');