%% Check Box Edges

Sw = 20;
Sh = 20;

for p = 1:numPeople
    
    state(jj,p).preEnable(1,:) = false(1,numCam);
    
    if state(jj,p).enable
        
        if ~isempty(state(jj,p).box)
            
            if pixelW - state(jj,p).box(1) < Sw || pixelH - state(jj,p).box(2) < Sh
                
                for m = 2:numCam
                    
                    if logical(V(jj-1,m-1))
                        
                        state(jj,p).preEnable(m) = true;
                        
                    end
                    
                end
                
            else
                
                if state(jj,p).box(1)+state(jj,p).box(3) < Sw || state(jj,p).box(2)+state(jj,p).box(4) < Sh
                    
                    for m = 2:numCam
                        
                        if logical(V(jj-1,m-1))
                             
                            state(jj,p).preEnable(m) = true;
                                
                        end
                        
                    end
                end
                
            end
            
        end
        
    end
    
end