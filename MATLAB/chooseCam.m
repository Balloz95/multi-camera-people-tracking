%% CHOOSE CAMERA

for p = 1:numPeople
    
    for cam = 2:numCam

        %check if the cameras see the same thing and they can talk each other
        if numPeople > 0 && cam ~= jj

            if state(cam,p).enable && state(jj,p).enable

                if state(cam,p).foundFace

                    if state(jj,p).foundFace

                        areaA  = state(cam,p).box(3) * state(cam,p).box(4);
                        areajj  = state(jj,p).box(3) * state(jj,p).box(4);

                        if areaA > areajj

                            state(jj,p).enable = false;

                        else

                            state(cam,p).enable = false;

                        end

                    else

                        state(jj,p).enable = false;

                    end

                else

                    if state(jj,p).foundFace

                        state(cam,p).enable = false;

                    end

                end

            end

        end

    end
    
end