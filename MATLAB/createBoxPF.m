for p = 1:numPeople
    
    if state(jj,p).enable
    
        x = state(jj,p).X(1,L2(p,:) > thBoxPF);
        y = state(jj,p).X(2,L2(p,:) > thBoxPF);
        minX = min(x);
        minY = min(y);
        w = max(x) - minX;
        h = max(y) - minY;
        state(jj,p).box = [minY, minX, h, w];
    
    end
    
end