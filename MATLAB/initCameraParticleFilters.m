%-------------------------------------------------------------------------%
%                 INITIALIZATION FOR PARTICLE FILTERS                     %
%-------------------------------------------------------------------------%

% Number of particles
N_particles = 500;

% Structure which contains the state of each particle filter for each
% camera and person
state = struct('X',[],...
               'box', [],...
               'preEnable', [],...
               'enable', true,...
               'foundFace', false,...
               'lostFace', 0);

% Standard deviation of the color
Xstd_rgb = 15;

% Standard deviation of the position
Xstd_pos = 25; 

%standard deviation of the velocity
Xstd_vec = 5;

% Transfer Function
F_update = [1 0 1 0; 0 1 0 1; 0 0 1 0; 0 0 0 1];

% Recognized/unrecognized face threshold
faceMatchTh = 1;

% Default spatial penalizing factor
defaultSpatialDiff = 1000;

% Weight for convex combination between colour and spatial difference for
% logLikeihood computation
wColour = 0.2;
