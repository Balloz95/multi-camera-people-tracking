%-------------------------------------------------------------------------%
%                 INITIALIZATION OF THE MAIN LOOP                         %
%-------------------------------------------------------------------------%

% Number of the first internal camera
jj = 2;

% Number of the frame in the main loop
numF = 1;

% Threshold used to compute the background during the tracking
thPF = th;

% Threshold used to create the box around the particles
thBoxPF = 0.5;

% Frames to wait before calling Lost Face
numFrameLostFace = 10;

% Tracking box colour indicating confidence level (face or non-face)
confidenceColor = 'y';

% Video player
videoPlayer = vision.VideoPlayer;

% Video writer to save tests
if strcmp(writeOtputVideo,'y')

    videoWriter = VideoWriter(strcat(repoVideo,'\',testName,'.avi'));
    
end