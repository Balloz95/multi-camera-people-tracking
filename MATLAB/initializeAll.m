%% Initialization Scripts

%Initialize the cameras
initCameraSetting;

%Initialize the Background of each camera
initCameraBackground;

%Initialize the params to create the classes
initClassCreationParams;

%Initialize the params to compute the color dominant
initCameraColorDominant;

%Initialize the particle filters
initCameraParticleFilters;

%Initialize the params for the main loop of the program
initCameraMainLoop;

%Initialize and set the Face Detector
initCameraCropFaces;