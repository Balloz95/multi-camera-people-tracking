framePF = frame;
frametmp = double(frame);

diff = (abs(frametmp(:,:,1)-background(1,1).Image(:,:,1))>thPF) | ...
             (abs(frametmp(:,:,2)-background(1,1).Image(:,:,2))>thPF) | ...
             (abs(frametmp(:,:,3)-background(1,1).Image(:,:,3))>thPF);
         
diff = imclose(diff, closeKernel);

framer = frame(:,:,1);
frameg = frame(:,:,2);
frameb = frame(:,:,3);

framer(~diff) = colorBG(1);
frameg(~diff) = colorBG(2);
frameb(~diff) = colorBG(3);

framePF(:,:,1) = framer;
framePF(:,:,2) = frameg;
framePF(:,:,3) = frameb;