fprintf('\n%-25s \t %-3s \t %-10s \n','BACKGROUND ANALISYS','-->','START');

% Background structure
background(1:numCam) = struct('Frame',[],...
                    'Temp',zeros(pixelH, pixelW, 3),...
                    'Image',zeros(pixelH, pixelW, 3));

for ii = 1:numFrameBg
    
    for k = 1:numCam

        background(1,k).Frame = readFrame(video{1,k});        
        background(1,k).Temp = double(background(1,k).Frame) + background(1,k).Temp;
    
    end
    
end

for k = 1:numCam
    
        background(1,k).Image = background(1,k).Temp ./ numFrameBg;
        
end

fprintf('%-25s \t %-3s \t %-10s \n','BACKGROUND ANALISYS','-->','FINISH');