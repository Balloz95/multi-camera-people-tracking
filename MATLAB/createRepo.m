fprintf('%-25s \t %-3s \t %-10s \n','DIRECTORIES','-->','START');

if isfolder('faces')
    
    rmdir faces s;
    
end

if isfolder('output_video')
    
    rmdir output_video s;
    
end

mkdir faces;
repo = 'faces\';
prompt = 'Would you like to save the output video? (y/n)';
writeOtputVideo = input(prompt,'s');

if strcmp(writeOtputVideo,'y')
    
    mkdir output_video;
    repoVideo = 'output_video\';
    
end

fprintf('%-25s \t %-3s \t %-10s \n','DIRECTORIES','-->','FINISH');