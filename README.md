# File description

1. All of the MATLAB scripts are inside the related folder. To run the program, it is sufficient to run the main script and follow the instructions. In order to exploit the test video files, enter:
   1.1 "3" as total # of cameras;
   1.2 "test<i>", i = 1,2,3.

2. The final report is a detailed, IEEE-like paper reporting all the procedure and results.

3. The PowerPoint presentation both recaps the procedure and the algorithms used and shows some experiments.

